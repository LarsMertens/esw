package bean;

import javax.inject.Named;
import javax.enterprise.context.Dependent;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.servlet.http.HttpSession;
import model.User;
import service.UserService;

@Named
@Dependent
public class AuthBean {
    
    @Inject
    private UserService userService;

    public String doLogout() {
        ((HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(false)).invalidate();

        return "/auth/login.xhtml?faces-redirect=true";
    }

    public boolean isLoggedIn() {
        return FacesContext.getCurrentInstance().getExternalContext().getUserPrincipal() != null;
    }

    public String getPrincipalName() {
        if (isLoggedIn()) {
            return FacesContext.getCurrentInstance().getExternalContext().getUserPrincipal().getName();
        } else {
            return "ANONYMOUS";
        }
    }
    
    public User getUser(){
        if (isLoggedIn()) {
            String username = FacesContext.getCurrentInstance().getExternalContext().getUserPrincipal().getName();
            return userService.getUserByName(username);
        } else {
            return null;
        }
    }
    
    public boolean isAdmin() {
        FacesContext fc = FacesContext.getCurrentInstance();
        return fc.getExternalContext().isUserInRole("AdminRole");
    }
}
