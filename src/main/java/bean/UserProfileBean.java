package bean;

import dao.GroupDaoJPA;
import java.beans.Statement;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import model.User;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import javax.annotation.ManagedBean;
import javax.annotation.PostConstruct;
import javax.el.ELContext;
import javax.inject.Named;
import javax.enterprise.context.RequestScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.persistence.Transient;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import model.Group;
import model.Profile;
import model.Tweet;
import org.primefaces.model.UploadedFile;
import service.ProfileService;
import service.UserService;
import util.PasswordHash;

@Named(value = "userProfileBean")
@RequestScoped
@ManagedBean
public class UserProfileBean implements Serializable{
    
    @Inject
    private UserService userService;
    
    @Inject
    private ProfileService profileService;
    
    @Inject
    private GroupDaoJPA groupDao;
    
    private Long id;
    @Size(min=3, max=160)
    private String username;
    @Pattern(regexp = "^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\\.[a-zA-Z0-9-.]+$", message = "This is not a valid email")
    private String email;
    @Size(min=3, max=160)
    private String password;
    @Size(min=3, max=160)
    private String type;
    @Size(min=3, max=160)
    private String firstname;
    @Size(min=3, max=160)
    private String lastname;
    @Size(min=3, max=160)
    private String dateString;
    private Date date_of_birth;
    @Size(min=3, max=160)
    private String biography;
    @Size(max=160)
    private String location;
    @Size(max=160)
    private String website;
    @NotNull
    private String avatar;
    @NotNull
    private String image;
    private Date created_at;
    private Date updated_at;
    @NotNull
    private UploadedFile file;
    @NotNull
    private UploadedFile imageFile;
    private Collection<Group> group = new ArrayList<>();
    private User user;
    private Profile profile;
   
    public void RegisterUser() throws FileNotFoundException, IOException, ParseException, ClassNotFoundException, SQLException{
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        String strDate = dateString;
        date_of_birth = new Date(sdf.parse(dateString).getTime());
        
        User user = new User(username, email, PasswordHash.stringToHash(password), "user", firstname, lastname, date_of_birth);
        user.setCreated_at(new Date());
        user.setUpdated_at(new Date());
        //user.getGroup().add(groupDao.find(Group.ADMIN_GROUP));
        userService.addUser(user);
        
        UploadedFile uploadedPhoto = getFile();
        UploadedFile uploadedPhotoBackground = getImageFile();
        String filePath="c:/Users/lars_/Documents/NetBeansProjects/Kwetter/src/main/webapp/uploads/";
        String filename;
        Profile profile = new Profile();
        byte[] bytes=null;
 
        if (null!=uploadedPhoto) {
            bytes = uploadedPhoto.getContents();
            filename = uploadedPhoto.getFileName();
            BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(new File(filePath+filename)));
            stream.write(bytes);
            stream.close();
            profile.setAvatar(filename);
            
            bytes = uploadedPhotoBackground.getContents();
            filename = uploadedPhotoBackground.getFileName();
            stream = new BufferedOutputStream(new FileOutputStream(new File(filePath+filename)));
            stream.write(bytes);
            stream.close();
            profile.setImage(filename);
        }
        profile.setBiography(biography);
        profile.setLocation(location);
        profile.setWebsite(website);
        profile.setCreated_at(new Date());

        profileService.addProfile(profile);
        user.setProfile(profile);
        profile.setUser(user);
    }

    public void insertGroup(String username, String userGroup) throws ClassNotFoundException, SQLException{
        String driver="com.mysql.jdbc.Driver";
        String url="jdbc:mysql://localhost:3306/esw";
        String uname="root";
        String pass="";
        Class.forName(driver);
        Connection c= DriverManager.getConnection(url,uname,pass);
        PreparedStatement pstmt = c.prepareStatement("INSERT INTO `users_groups`(username,groupname) VALUES (?, ?)");
        pstmt.setString(1, username);
        pstmt.setString(2, userGroup);
        pstmt.executeUpdate();
    }
    
    public UserService getUserService() {
        return userService;
    }

    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    public ProfileService getProfileService() {
        return profileService;
    }

    public void setProfileService(ProfileService profileService) {
        this.profileService = profileService;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public Date getDate_of_birth() {
        return date_of_birth;
    }

    public void setDate_of_birth(Date date_of_birth) {
        this.date_of_birth = date_of_birth;
    }

    public String getBiography() {
        return biography;
    }

    public void setBiography(String biography) {
        this.biography = biography;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public Date getCreated_at() {
        return created_at;
    }

    public void setCreated_at(Date created_at) {
        this.created_at = created_at;
    }

    public Date getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(Date updated_at) {
        this.updated_at = updated_at;
    }

    public UploadedFile getFile() {
        return file;
    }

    public void setFile(UploadedFile file) {
        this.file = file;
    }

    public UploadedFile getImageFile() {
        return imageFile;
    }

    public void setImageFile(UploadedFile imageFile) {
        this.imageFile = imageFile;
    }

    public String getDateString() {
        return dateString;
    }

    public void setDateString(String dateString) {
        this.dateString = dateString;
    }

    public GroupDaoJPA getGroupDao() {
        return groupDao;
    }

    public void setGroupDao(GroupDaoJPA groupDao) {
        this.groupDao = groupDao;
    }

    public Collection<Group> getGroup() {
        return group;
    }

    public void setGroup(Collection<Group> group) {
        this.group = group;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Profile getProfile() {
        return profile;
    }

    public void setProfile(Profile profile) {
        this.profile = profile;
    }
    
    
}
