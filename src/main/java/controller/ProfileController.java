package controller;

import java.util.Date;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import model.Profile;
import service.ProfileService;
import service.UserService;

@Path("profile")
@Stateless
public class ProfileController {
    
    @Inject
    private ProfileService profileService;
    
    @Inject
    private UserService userService;
   
    public ProfileController(){}
    
    /**
     * Gets an profile from an user
     * @param id - id of the user
     * @return Profile from user
     */
    @GET
    @Path("get/{id}")
    @Produces({MediaType.APPLICATION_JSON})
    public Profile getProfile(@PathParam("id") Long id){
        return userService.getUser(id).getProfile();
    }
    
    /**
     * Creates an profile for an user
     * @param profile - profile that is going to be created for this user
     * @param user_id - id of the affected user
     */
    @POST
    @Path("/registerProfile/{user_id}")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public void registerProfile(Profile profile, @PathParam("user_id") Long user_id){
        //profile.setUser_id(user_id);
        profileService.addProfile(profile);
    }
    
    /**
     * Edits the profile from an user
     * @param profile - profile that gots to be edited from user
     * @param id - id of the profile
     * @return updated profile
     */
    @POST
    @Path("edit/{id}")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public Profile editProfile(Profile profile, @PathParam("id") Long id){
        Profile updatedProfile = profileService.getProfile(id);
        updatedProfile.setBiography(profile.getBiography());
        updatedProfile.setLocation(profile.getLocation());
        updatedProfile.setWebsite(profile.getWebsite());
        updatedProfile.setAvatar(profile.getAvatar());
        updatedProfile.setImage(profile.getImage());
        updatedProfile.setUpdated_at(new Date());
        return profileService.updateProfile(updatedProfile);
    }
}
