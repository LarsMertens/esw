package controller;

import java.util.ArrayList;
import static java.util.Collections.list;
import java.util.Date;
import java.util.List;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import model.Tweet;
import service.TweetService;
import service.UserService;

@Path("tweet")
@Stateless
public class TweetController {
    
    @Inject
    private TweetService tweetService;
    
    @Inject
    private UserService userService;
    
    public TweetController(){}
    
    /**
     * Adds an Tweet 
     * @param tweet - adds a tweet
     * @param user_id - id from user
     * @return tweet
     */
    @POST
    @Path("/add/{user_id}")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public Tweet addTweet(Tweet tweet, @PathParam("user_id") Long user_id){
        tweet.setCreated_at(new Date());
        tweet.setUpdated_at(new Date());
        //tweet.setUser(userService.getUser(user_id));
        tweetService.addTweet(tweet, user_id);
        return tweet;
    }
    
    /**
     * Edit a Tweet
     * @param tweet - tweet that got to be edited
     * @param id - id of the tweet
     * @return updated tweet
     */
    @POST
    @Path("/edit/{id}")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public Tweet editTweet(Tweet tweet, @PathParam("id") Long id){
        Tweet updatedTweet = tweetService.getTweet(id);
        updatedTweet.setSubject(tweet.getSubject());
        updatedTweet.setContent(tweet.getContent());
        updatedTweet.setUpdated_at(new Date());
        if(tweet.isTrent()){
            updatedTweet.setTrent(true);
        } else {
            updatedTweet.setTrent(false);
        }
        return updatedTweet;
    }
    
    /**
     * Delete a tweet
     * @param id - id of a tweet
     */
    @POST
    @Path("/delete/{id}")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public void deleteTweet(@PathParam("id") Long id){
        tweetService.deleteTweet(id);
    }
    
    /**
     * Search a tweet
     * @param input - input string which will search in content
     * @return list of tweets with given input %like%
     */
    @POST
    @Path("/search")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public List<Tweet> searchTweets(@HeaderParam("input") String input){
        return tweetService.searchTweets(input);
    }
    
    /**
     * Get all the trents
     * @return list of tweets that are trents
     */
    @GET
    @Path("/trends")
    @Produces({MediaType.APPLICATION_JSON})
    public List<Tweet> getTrends(){
        return tweetService.getTrends();
    }
}
