package controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import model.Profile;
import model.Tweet;
import model.User;
import service.ProfileService;
import service.TweetService;
import service.UserService;

@Path("user")
@Stateless
public class UserController {
    
    @Inject
    private UserService userService;
    
    @Inject
    private ProfileService profileService;
    
    @Inject
    private TweetService tweetService;
   
    public UserController(){}
    
    /**
     * Register a user
     * @param user - user that is going to be created
     */
    @POST
    @Path("/register")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public void registerUser(User user){
        userService.addUser(user);
    }
    
    /**
     * Edit an user
     * @param user - user that got to be updated
     * @param id - id from the user
     * @return updated user
     */
    @POST
    @Path("/edit/{id}")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public User editUser(User user, @PathParam("id") Long id){
        User updatedUser = userService.getUser(id);
        updatedUser.setUsername(user.getUsername());
        updatedUser.setEmail(user.getEmail());
        updatedUser.setType(user.getType());
        updatedUser.setFirstname(user.getFirstname());
        updatedUser.setLastname(user.getLastname());   
        updatedUser.setUpdated_at(new Date());
        return updatedUser;
    }
    
    /**
     * Get the latest tweets from an user
     * @param id - id from the user
     * @return latest tweets
     */
    @GET
    @Path("/getLatestTweets/{id}")
    @Produces({MediaType.APPLICATION_JSON})
    public List<Tweet> getLatestTweets(@PathParam("id") Long id){
        return userService.getLatestTweets(id);
    }
    
    /**
     * Get all followers by given user id
     * @param id - id from the user
     * @return all users that are followers from given user id
     */
    @GET
    @Path("/followers/{id}")
    @Produces({MediaType.APPLICATION_JSON})
    public List<User> getFollowers(@PathParam("id") Long id){
        return userService.getFollowers(id);
    }
    
    /**
     * Add follower by user id
     * @param my_id - my user id
     * @param user_id - user id that need to be a follower
     */
    @POST
    @Path("/followers/add/{my_id}/{user_id}")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public void addFollower(@PathParam("my_id") Long my_id, @PathParam("user_id") Long user_id){
        userService.addFollower(my_id, user_id);
    }
    
    /**
     * Removes follower by user id
     * @param my_id - my user id
     * @param user_id - user id that need to be a follower
     */
    @POST
    @Path("/followers/delete/{my_id}/{user_id}")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public void deleteFollower(@PathParam("my_id") Long my_id, @PathParam("user_id") Long user_id){
        userService.removeFollower(my_id, user_id);
    }
    
    /**
     * Gets a list of all following by user id
     * @param id - id from the user
     * @return all following from the given user
     */
    @GET
    @Path("/following/{id}")
    @Produces({MediaType.APPLICATION_JSON})
    public List<User> getFollowing(@PathParam("id") Long id){
        return userService.getFollowing(id);
    }
    
    /**
     * Adds an following to an user
     * @param my_id - user id of me
     * @param user_id - user id of the user that needs to be following
     */
    @POST
    @Path("/following/add/{my_id}/{user_id}")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public void addFollowing(@PathParam("my_id") Long my_id, @PathParam("user_id") Long user_id){
        userService.addFollowing(my_id, user_id);
    }
    
    /**
     * Removes following by user id
     * @param my_id - my user id
     * @param user_id - user id that need to be a following
     */
    @POST
    @Path("/following/delete/{my_id}/{user_id}")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public void deleteFollowing(@PathParam("my_id") Long my_id, @PathParam("user_id") Long user_id){
        userService.removeFollowing(my_id, user_id);
    }

    /**
     * Search users by username
     * @param input - input as string for username
     * @return list of users
     */
    @POST
    @Path("/search")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public List<User> searchUsers(@HeaderParam("input") String input){
        return userService.searchUsers(input);
    }

    /**
     * Get timeline from user
     * @param id - id from user
     * @return timeline from user
     */
    @GET
    @Path("/timeline/{id}")
    @Produces({MediaType.APPLICATION_JSON})
    public List<Tweet> getTimeline(@PathParam("id") Long id){
        //userService.getUser(id).setTweets(tweetService.getTweets());
        return userService.getTimeline(id);
    }
    
    /**
     * Get all mentions from user
     * @param id - user id
     * @return all mentions
     */
    @GET
    @Path("/mentions/{id}")
    @Produces({MediaType.APPLICATION_JSON})
    public List<Tweet> getMentions(@PathParam("id") Long id){
        return userService.getMentions(id);      
    }
    
    /**
     * Set role of user
     * @param id - user id
     * @param type - set user type
     */
    @POST
    @Path("/setUserRole")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public void setUserRole(@HeaderParam("id") Long id, @HeaderParam("type") String type){
        userService.getUser(id).setType(type);
    }
    
    /**
     * Add a tweet as favorite
     * @param user_id - is the user from the favorite tweet
     * @param tweet_id  - is the tweet that got to be favorited
     */
    @POST
    @Path("/addFavorite/{user_id}/{tweet_id}")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public void addFavoriteTweet(@PathParam("user_id") Long user_id, @PathParam("tweet_id") Long tweet_id){
        //userService.addFavoriteTweet(user_id, tweet_id);
    }
    
    /**
     * Remove a tweet as favorite
     * @param user_id - is the user from the favorite tweet
     * @param tweet_id  - is the tweet that got to be favorited
     */
    @POST
    @Path("/removeFavorite/{user_id}/{tweet_id}")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public void removeFavoriteTweet(@PathParam("user_id") Long user_id, @PathParam("tweet_id") Long tweet_id){
        //userService.removeFavoriteTweet(user_id, tweet_id);
    }
    
    /**
     * Login user
     * @param email - email from user
     * @param password - password from user
     * @return true - no authentication layer yet
     */
    @POST
    @Path("/login")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public boolean loginUser(@HeaderParam("email") String email, @HeaderParam("password") String password){
        // @TODO : requires authentication DB - so just free pass now
        return true;
    }
    
    /**
     * Logout user
     * @return true - no authentication layer yet
     */
    @POST
    @Path("/logout")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public boolean logoutUser(){
        // @TODO : requires authentication DB - so just free pass now
        return true;
    }
}
