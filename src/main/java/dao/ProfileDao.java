package dao;

import java.util.List;
import model.Profile;

public interface ProfileDao {
    
    /**
     * Creates a profile model
     * @param p = profile object instance
     */
    void create(Profile p);

    /**
     * Get all profiles
     * @return a List of profile model objects
     */
    List<Profile> findAll();

    /**
     * Get a single profile by id
     * @param id - id of the profile
     * @return a single profile model object
     */
    Profile find(Long id);
    
    /**
     * The profile that need to be updated
     * @param transientProfile - updating profile
     * @return updated profile
     */
    Profile update (Profile transientProfile);
}
