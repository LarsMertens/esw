package dao;

import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import model.Profile;

@Stateless
@JPA
public class ProfileDaoJPA implements ProfileDao{
    
    @PersistenceContext(unitName = "KwetterPU")
    private EntityManager em;
    
    public ProfileDaoJPA(){}

    @Override
    public void create(Profile p) {
        em.persist(p);
    }
    
    @Override
    public Profile update (Profile transientProfile) {
        return em.merge(transientProfile);
    }

    @Override
    public List<Profile> findAll() {
        Query query = em.createQuery("SELECT p FROM Profile p");
        return new ArrayList<>(query.getResultList());
    }

    @Override
    public Profile find(Long id) {
        TypedQuery<Profile> query = em.createNamedQuery("profile.findById", Profile.class);
        query.setParameter("id", id);
        List<Profile> result = query.getResultList();
        System.out.println("count: " + result.size());
        return result.get(0);
    }
}
