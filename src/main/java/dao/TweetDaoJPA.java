package dao;

import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.enterprise.inject.Default;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import model.Profile;
import model.Tweet;
import model.User;

@Stateless 
@JPA
public class TweetDaoJPA implements TweetDao{

    @PersistenceContext(unitName = "KwetterPU")
    private EntityManager em;
    
    @Override
    public boolean create(Tweet t, Long id) {
        TypedQuery<User> query = em.createNamedQuery("user.findById", User.class);
        query.setParameter("id", id);
        List<User> result = query.getResultList();
        User user = result.get(0);    
        
        user.getTweets().add(t);
        user.setTweets(user.getTweets());
        return true;
    }

    @Override
    public List<Tweet> findAll() {
        Query query = em.createQuery("SELECT t FROM Tweet t");
        return new ArrayList<>(query.getResultList());
    }

    @Override
    public Tweet find(Long id) {
        TypedQuery<Tweet> query = em.createNamedQuery("tweet.findById", Tweet.class);
        query.setParameter("id", id);
        List<Tweet> result = query.getResultList();
        System.out.println("count: " + result.size());
        return result.get(0);    
    }

    @Override
    public List<Tweet> search(String input) {
        System.out.println("MYINPUT: " + input);
        TypedQuery<Tweet> query = em.createNamedQuery("tweet.findByContent", Tweet.class);
        query.setParameter("content", "%" + input + "%");
        List<Tweet> result = query.getResultList();
        System.out.println("count: " + result.size());
        return result;
    }

    @Override
    public List<Tweet> getTrends() {
        TypedQuery<Tweet> query = em.createNamedQuery("tweet.findTrents", Tweet.class);
        List<Tweet> result = query.getResultList();
        System.out.println("count: " + result.size());
        return result;
    }
    
    /**
     * Deletes an tweet
     * @param id - tweet id
     */
    @Override
    public void deleteTweet(Long id) {
        em.remove(em.find(Tweet.class, id));
    }
    
    @Override
    public void addFavoriteTweet(Long user_id, Long tweet_id) {       
        TypedQuery<User> query = em.createNamedQuery("user.findById", User.class);
        query.setParameter("id", user_id);
        List<User> result = query.getResultList();
        User user = result.get(0);    
        
        user.getFavorites().add(this.find(tweet_id));
        user.setFavorites(user.getFavorites());
    }
    
    @Override
    public void removeFavoriteTweet(Long user_id, Long tweet_id) {       
        TypedQuery<User> query = em.createNamedQuery("user.findById", User.class);
        query.setParameter("id", user_id);
        List<User> result = query.getResultList();
        User user = result.get(0);    
        
        user.getFavorites().remove(this.find(tweet_id));
        user.setFavorites(user.getFavorites());
    }
    
    @Override
    public boolean isFavoriteTweet(Long user_id, Long tweet_id) {
        Query query = em.createQuery("select t " + 
                                     "from User u " +
                                     "join u.favorites t " +
                                     "where u.id = ?1 " +
                                     "and t.id = ?2 ");
        query.setParameter( 1, user_id ); 
        query.setParameter( 2, tweet_id );
        return query.getResultList().isEmpty();
    }

    @Override
    public List<Tweet> getFavoriteTweets(Long user_id){
        Query query = em.createQuery("select t " + 
                                     "from User u " +
                                     "join u.favorites t " +
                                     "where u.id = ?1 ");
        query.setParameter( 1, user_id ); 
        //query.setParameter( 2, tweet_id );
        return new ArrayList<>(query.getResultList());
    }
}
