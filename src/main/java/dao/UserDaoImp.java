package dao;

import helper.UserValidation;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import javax.ejb.Stateless;
import javax.enterprise.inject.Default;
import model.Profile;
import model.Tweet;
import model.User;

@Stateless @Default
public class UserDaoImp implements UserDao {
    
    private HashMap<Long, User> users;
    private Long nextId;
    private TweetDao tweetDao;
    
    /**
     * This class will be used as data access layer and to create 
     * user objects in memory
     */
    public UserDaoImp(){
        this.nextId = 1L;
        this.users = new HashMap<Long, User>();
        this.tweetDao = new TweetDaoImp();
        initUsers();
    }
    
    /**
     * Initialise an in memory Hashmap of 10 user objects
     */
    public void initUsers(){
        for(int i = 1; i < 11; i++){
           Long id = new Long(i); 
           User user = new User("username" + id.toString(),"email"+id.toString()+"@email.com",
                                "password" + id.toString(),"type" + id.toString(),
                                "firstname" + id.toString(), "lastname" + id.toString(), new Date());  
           this.create(user);
           if(id == 8){
               Profile profile = new Profile("biography" + id.toString(), "location" + id.toString(), 
                                         "website" + id.toString(), "../avatar.jpg" + id.toString(), 
                                         "../image.jpg" + id.toString());
               user.setProfile(profile);
           }
           id++;
        }
       
    }

    /**
     * Creates a user object
     * if no user is selected throw an IllegalArgumentException
     * if user email already exist throw an IllegalArgumentException
     * 
     * @param u = the selected user object
     */
    @Override
    public void create(User u) {
        if (u == null) {
            throw new IllegalArgumentException("User is null");
        }
        for(int i = 1; i <= this.users.size(); i++){
          if(this.find(new Long(i)).getEmail().equals(u.getEmail())){
             throw new IllegalArgumentException("Email already exists.");
          } else if(this.find(new Long(i)).getUsername().equals(u.getUsername())){
             throw new IllegalArgumentException("Username already exists.");
          }
        }
        UserValidation.validateEmail(u.getEmail());
        u.setId(this.nextId);
        this.users.put(this.nextId, u);
        this.nextId++;
    }

    /**
     * Get all users from a hashmap
     * @return all users as an ArrayList
     */
    @Override
    public List<User> findAll() {
       return new ArrayList(this.users.values()); 
    }

    /**
     * Get a single user object from a hashmap
     * If a user is not found by given id
     * @param id - id of the selected user
     * @return a single user object
     */
    @Override
    public User find(Long id) {
        if (!this.users.containsKey(id)) {
            throw new IllegalArgumentException("Id no found" + id);
        }
        return this.users.get(id);
    }

    /**
     * Search for users by username
     * @param input - username for user
     * @return list of users
     */
    @Override
    public List<User> search(String input) {
        ArrayList<User> foundUsers = new ArrayList<User>();
        for(int i = 1; i <= this.users.size(); i++){
            if(this.find(new Long(i)).getUsername().contains(input)){
                foundUsers.add(this.find(new Long(i)));
            }
        }
        return foundUsers;
    }

    @Override
    public List<Tweet> getTimeline(Long id) {
        ArrayList<Tweet> timelineTweets = (ArrayList<Tweet>) this.find(id).getTweets();
        
        for(int i = 0; i < this.find(id).getFollowing().size(); i++){
            for(int y = 0; y < this.find(id).getTweets().size(); y++){
                timelineTweets.add(this.find(id).getTweets().get(y));
            }
        }
        
        Collections.sort(timelineTweets, Collections.reverseOrder());
        return timelineTweets;
    }

    /**
     * @TODO : Return latest tweets for in memory
     * @param id - user id
     * @return latest tweets
     */
    @Override
    public List<Tweet> getLatestTweets(Long id) {
        return null;
    }

    /**
     * Gets a list of all followers by user id
     * @param id - id from the user
     * @return all followers from the given user
     */
    @Override
    public List<User> getFollowers(Long id) {
        return this.find(id).getFollowers();
    }

    /**
     * Adds an follower to an user
     * @param my_id - user id of me
     * @param user_id - user id of the user that needs to be followed
     */
    @Override
    public void addFollower(Long my_id, Long user_id) {
        this.find(my_id).getFollowers().add(this.find(user_id));
    }
    
    /**
     * Removes follower by user id
     * @param my_id - my user id
     * @param user_id - user id that need to be a follower
     */
    public void removeFollower(Long my_id, Long user_id){
        this.find(my_id).getFollowers().remove(this.find(user_id));
    }
    
    /**
     * Gets a list of all following by user id
     * @param id - id from the user
     * @return all following from the given user
     */
    @Override
    public List<User> getFollowing(Long id) {
        return this.find(id).getFollowing();
    }

     /**
     * Adds an following to an user
     * @param my_id - user id of me
     * @param user_id - user id of the user that needs to be following
     */
    @Override
    public void addFollowing(Long my_id, Long user_id) {
        this.find(my_id).getFollowing().add(this.find(user_id));
    }
    
    /**
     * Removes following by user id
     * @param my_id - my user id
     * @param user_id - user id that need to be a following
     */
    public void removeFollowing(Long my_id, Long user_id){
        this.find(my_id).getFollowing().remove(this.find(user_id));
    }

    /**
     * Get all mentions from an user
     * @param id - an user id
     * @return all mentions from an user 
     */
    @Override
    public List<Tweet> getMentions(Long id) {
        return this.find(id).getMentions();
    }

    @Override
    public List<Tweet> getFollowingTweets(Long id) {
        return null;
    }

    @Override
    public boolean isFollowing(Long my_id, Long following_id) {
        return true;
    }

    @Override
    public User getUserByName(String username) {
        return null;
    }

    @Override
    public void addMention(Long user_id, Long tweet_id) {
       
    }
    
    @Override
    public User getUserByTweetID(Long tweet_id){
        return null;
    }
}
