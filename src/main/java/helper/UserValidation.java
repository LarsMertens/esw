package helper;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class UserValidation {
    
    public UserValidation(){}
    
    /**
     * Validate if email is valid
     * @param email - given email
     * @return true or exception
     */
    public static boolean validateEmail(String email){
        Pattern regexPattern = Pattern.compile("^[(a-zA-Z-0-9-\\_\\+\\.)]+@[(a-z-A-z)]+\\.[(a-zA-z)]{2,3}$");
        Matcher regMatcher = regexPattern.matcher(email);
        if(regMatcher.matches()){
            return true;
        } else{
           throw new IllegalArgumentException("Email is not a valid email");
        }
    }
}
