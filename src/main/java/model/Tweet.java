package model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OrderBy;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@NamedQueries({
    @NamedQuery(name = "tweet.findById", query = "SELECT t FROM Tweet t WHERE t.id = :id"),
    @NamedQuery(name = "tweet.findByContent", query = "SELECT t FROM Tweet t WHERE t.content LIKE :content"),
    @NamedQuery(name = "tweet.findTrents", query = "SELECT t FROM Tweet t WHERE t.trent = true")
})

@XmlRootElement
public class Tweet implements Comparable<Tweet>, Serializable{
    
    @Id @GeneratedValue
    private Long id;
    
    //@ManyToOne
    // private Long user_id;
    
    private String subject;
    private String content;
    private Date created_at;
    private Date updated_at;
    private boolean trent;
    
    @ManyToMany(mappedBy = "tweets", cascade = CascadeType.PERSIST)
    private List<User> users;
    
    @ManyToMany(mappedBy = "favorites", cascade = CascadeType.PERSIST)
    private List<User> favoriteUsers;
    
    @ManyToMany(mappedBy = "mentions", cascade = CascadeType.PERSIST)
    private List<User> mentionsUsers;

    public Tweet(){}
    
    public Tweet(Long user_id, String subject, String content) {
        this.id = id;
        //this.user_id = user_id;
        this.subject = subject;
        this.content = content;
        this.created_at = new Date();
        this.updated_at = new Date(); 
        this.trent = trent;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Date getCreated_at() {
        return created_at;
    }

    public void setCreated_at(Date created_at) {
        this.created_at = created_at;
    }

    public Date getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(Date updated_at) {
        this.updated_at = updated_at;
    } 

    public boolean isTrent() {
        return trent;
    }

    public void setTrent(boolean trent) {
        this.trent = trent;
    }

    public List<User> getUsers() {
        return users;
    }

    public void setUsers(List<User> users) {
        this.users = users;
    }

    public List<User> getFavoriteUsers() {
        return favoriteUsers;
    }

    public void setFavoriteUsers(List<User> favoriteUsers) {
        this.favoriteUsers = favoriteUsers;
    }

    @Override
    public int compareTo(Tweet t) {
        return getCreated_at().compareTo(t.getCreated_at());
    }
}
