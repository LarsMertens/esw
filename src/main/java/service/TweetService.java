package service;

import dao.JPA;
import dao.TweetDao;
import dao.TweetDaoImp;
import static java.lang.Math.toIntExact;
import java.util.List;
import javax.annotation.security.DeclareRoles;
import javax.annotation.security.RolesAllowed;
import javax.ejb.Stateless;
import javax.inject.Inject;
import model.Tweet;

@Stateless
@DeclareRoles({"UserRole", "AdminRole"})
public class TweetService {

    @Inject @JPA
    private TweetDao tweetDao;

    public TweetService() {
        tweetDao = new TweetDaoImp();
    }

    /**
     * Add a tweet
     * @param t - tweet
     * @return true or false
     */
    @RolesAllowed({"UserRole", "AdminRole"})
    public boolean addTweet(Tweet t, Long id) {
        tweetDao.create(t, id);
        return true;
    }

    /**
     * Get list of tweets
     * @return list of tweets
     */
    public List<Tweet> getTweets() {
        return tweetDao.findAll();
    }
    
    /**
     * Return list of tweets by searched input
     * @param input - search by content in tweets
     * @return list of tweets
     */
    public List<Tweet> searchTweets(String input){
        return tweetDao.search(input);
    }
    
    /**
     * Get single tweet
     * @param id - tweet id
     * @return selected tweet
     */
    public Tweet getTweet(Long id){
        return tweetDao.find(id);
    }
    
    /**
     * Delete a tweet
     * @param id - id of tweet that got to be removed
     */
    @RolesAllowed({"AdminRole"})
    public void deleteTweet(Long id){
        tweetDao.deleteTweet(id);
    }
    
    /**
     * Get trents of all tweets
     * @return list of tweets that are trents
     */
    public List<Tweet> getTrends(){
        return tweetDao.getTrends();
    }
    
    /**
     * Set a tweetDao
     * @param tweetDao - dao that got to be set as tweetdao 
     */
    public void setTweetDao(TweetDao tweetDao) {
        this.tweetDao = tweetDao;
    }
    
    /**
     * Adds an tweet as favorite
     * @param user_id - user id
     * @param tweet_id - tweet id
     */
    @RolesAllowed({"UserRole", "AdminRole"})
    public void addFavoriteTweet(Long user_id, Long tweet_id){
        tweetDao.addFavoriteTweet(user_id, tweet_id);
    }
    
    /**
     * Removes an tweet as favorite
     * @param user_id - user id
     * @param tweet_id - tweet id
     */
    @RolesAllowed({"UserRole", "AdminRole"})
    public void removeFavoriteTweet(Long user_id, Long tweet_id){
        tweetDao.removeFavoriteTweet(user_id, tweet_id);
    }
    
    @RolesAllowed({"UserRole", "AdminRole"})
    public boolean isFavoriteTweet(Long user_id, Long tweet_id){
        return tweetDao.isFavoriteTweet(user_id, tweet_id);
    }
    
    @RolesAllowed({"UserRole", "AdminRole"})
    public List<Tweet> getFavoriteTweets(Long user_id){
        return tweetDao.getFavoriteTweets(user_id);
    }
}
