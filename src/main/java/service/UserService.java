package service;

import dao.GroupDaoJPA;
import dao.JPA;
import dao.UserDao;
import dao.UserDaoImp;
import java.util.List;
import javax.annotation.security.DeclareRoles;
import javax.annotation.security.RolesAllowed;
import javax.ejb.Stateless;
import javax.inject.Inject;
import model.Group;
import model.Tweet;
import model.User;

@Stateless
@DeclareRoles({"UserRole", "AdminRole"})
public class UserService {

    @Inject @JPA
    private UserDao userDao;
    
    public UserService() {
        userDao = new UserDaoImp();
    }

    /**
     * Add an user
     * @param u - user that got to be added
     */
    public void addUser(User u) {
        userDao.create(u);
    }
    
    /**
     * Get list of users
     * @return list of users
     */
    public List<User> getUsers() {
        return userDao.findAll();
    }
    
    /**
     * Search users by username
     * @param input - username as string that got to be searched for
     * @return list of users
     */
    public List<User> searchUsers(String input){
        return userDao.search(input);
    }
    
    /**
     * Get a single user
     * @param id - user id
     * @return selected user
     */
    public User getUser(Long id){
        return userDao.find(id);
    }
    
    public User getUserByName(String username){
        return userDao.getUserByName(username);
    }
    
    /**
     * Get timeline from an user
     * @param id - id from user
     * @return list of tweets as timeline
     */
    public List<Tweet> getTimeline(Long id){
        return userDao.getTimeline(id);
    }
    
    /**
     * Get the latest 10 tweets from an user
     * @param id - id from the given user
     * @return list of tweets or empty array
     */
    public List<Tweet> getLatestTweets(Long id){
        return userDao.getLatestTweets(id);
    }
    
    /**
     * Gets a list of all followers by user id
     * @param id - id from the user
     * @return all followers from the given user
     */
    public List<User> getFollowers(Long id){
        return userDao.getFollowers(id);
    }
    
    /**
     * Adds an follower to an user
     * @param my_id - user id of me
     * @param user_id - user id of the user that needs to be followed
     */
    @RolesAllowed({"UserRole", "AdminRole"})
    public void addFollower(Long my_id, Long user_id){
        userDao.addFollower(my_id, user_id);
    }
    
    /**
     * Removes follower by user id
     * @param my_id - my user id
     * @param user_id - user id that need to be a follower
     */
    @RolesAllowed({"UserRole", "AdminRole"})
    public void removeFollower(Long my_id, Long user_id){
        userDao.removeFollower(my_id, user_id);
    }
    
    /**
     * Gets a list of all following by user id
     * @param id - id from the user
     * @return all following from the given user
     */
    public List<User> getFollowing(Long id){
        return userDao.getFollowing(id);
    }
    
    /**
     * Adds an following to an user
     * @param my_id - user id of me
     * @param user_id - user id of the user that needs to be following
     */
    @RolesAllowed({"UserRole", "AdminRole"})
    public void addFollowing(Long my_id, Long user_id){
        userDao.addFollowing(my_id, user_id);
    }
    
    /**
     * Removes following by user id
     * @param my_id - my user id
     * @param user_id - user id that need to be a following
     */
    @RolesAllowed({"UserRole", "AdminRole"})
    public void removeFollowing(Long my_id, Long user_id){
        userDao.removeFollowing(my_id, user_id);
    }
    
    /**
     * Get all mentions from an user
     * @param id - an user id
     * @return all mentions from an user 
     */
    public List<Tweet> getMentions(Long id){
        return userDao.getMentions(id);
    }
    
    /**
     * Adds an tweet as mention
     * @param user_id - user id
     * @param tweet_id - tweet id
     */
    @RolesAllowed({"UserRole", "AdminRole"})
    public void addMention(Long user_id, Long tweet_id){
        userDao.addMention(user_id, tweet_id);
    }
    
    public List<Tweet> getFollowingTweets(Long id){
        return userDao.getFollowingTweets(id);
    }
    
    public boolean isFollowing(Long my_id, Long following_id){
        return userDao.isFollowing(my_id, following_id);
    }
    
    public User getUserByTweetID(Long tweet_id){
        return userDao.getUserByTweetID(tweet_id);
    }
}
