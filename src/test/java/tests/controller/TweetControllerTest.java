package tests.controller;

import dao.ProfileDao;
import dao.TweetDao;
import dao.UserDao;
import static java.lang.Math.toIntExact;
import java.util.ArrayList;
import java.util.List;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import model.Profile;
import model.Tweet;
import model.User;
import static org.hamcrest.CoreMatchers.is;
import org.junit.After;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import org.mockito.runners.MockitoJUnitRunner;
import service.ProfileService;
import service.TweetService;
import service.UserService;

@RunWith(MockitoJUnitRunner.class)
public class TweetControllerTest {
   
    Client client;
    WebTarget root;
    static final String PATH = "/Kwetter/api/tweet/";
    static final String BASEURL = "http://localhost:8080" + PATH;
    List<Tweet> tweetList = new ArrayList<Tweet>();
    Tweet tweet;
    Tweet tweetTest;
     
    @Mock
    TweetDao tweetDao;
    
    @InjectMocks
    private TweetService tweetService;
    
    @Before
    public void setUp() {
        this.client = ClientBuilder.newClient();
        this.root = this.client.target(BASEURL);
        tweetTest = new Tweet(1L, "test", "content");
        tweet = new Tweet(1L, "test", "content");
        for(int i = 1; i <= 10; i++){
            tweetList.add(tweet);
        }
        tweetList.get(0).setId(3L);
    }

    @After
    public void tearDown() {
        tweetList = null;
    }    
    
    /**
     * Make a tweet and add to users tweet list.
     */
    @Test
    public void addTweetTest(){
        String mediaType = MediaType.APPLICATION_JSON;        
        final Entity<Tweet> entity = Entity.entity(tweetTest, mediaType);
        Tweet tweetResult = this.root.path("add").request().post(entity, Tweet.class);
        System.out.println("result entity:" + tweetTest.getId());
        System.out.println("result:" + tweetResult.getId());
        assertThat(tweetResult.getSubject(), is(tweetTest.getSubject()));
    }
        
    /**
     * Adds a tweet to an user favorite list
     * If tweet is added the size of the favorite list is 1
     */
//    @Test
//    public void addFavoriteTweetTest(){
//        Tweet t = new Tweet(1L, "subject11", "some text as content11");
//        tweetService.addTweet(t);
//        userService.getUsers().get(0).setFavorites(tweetService.getTweets());
//
//        if(userService.getUsers().get(0).getFavorites().size() == 11){
//           assertTrue(true);
//        } else {
//           assertFalse(true);
//        }  
//    }
//    
//    /**
//     * The tweet was already favorited
//     */
//    @Test
//    public void addFavoriteTweetAlreadyAddedTest(){
//        Tweet t = new Tweet(1L, "subject11", "some text as content11");
//        tweetService.addTweet(t);
//        userService.getUsers().get(0).setFavorites(tweetService.getTweets());
//        for(int i = 0; i < userService.getUsers().get(0).getTweets().size(); i++){
//            if(userService.getUsers().get(i).getId().equals(t.getId())){
//                assertTrue(true);
//            }
//        }
//    }
//    
//    /**
//     * Search an tweet if found more than 0 tweets this function will succeed
//     */
//    @Test
//    public void searchTweetTest(){
//        ArrayList<Tweet> searchedTweets = (ArrayList<Tweet>) tweetService.searchTweets("content");
//        if(searchedTweets.size() > 0){
//            assertTrue(true);
//        } else {
//            assertFalse(true);
//        }
//    }
//    
//    /**
//     * Search an tweet but found none
//     */
//    @Test
//    public void searchTweetButFoundNoneTest(){
//        ArrayList<Tweet> searchedTweets = (ArrayList<Tweet>) tweetService.searchTweets("nope");
//        if(searchedTweets.isEmpty()){
//            assertTrue(true);
//        } else {
//            assertFalse(true);
//        }
//    }
//    
//    /**
//     * Test if an user can get trends tweets
//     */
//    @Test
//    public void getTrendsTest(){
//        tweetService.getTweets().get(0).setTrent(true);
//        if(tweetService.getTweets().get(0).isTrent()){
//            assertTrue(true);
//        } else {
//            assertFalse(true);
//        }
//    }
//    
//    /**
//     * Test if admin can remove a tweet
//     */
//    @Test
//    public void removeTweetTest(){
//        ArrayList<User> users = (ArrayList<User>) userService.getUsers();
//        users.get(0).setTweets(tweetService.getTweets());
//        ArrayList<Tweet> tweets = (ArrayList<Tweet>) users.get(0).getTweets();
//        tweets.remove(0);
//        if(tweets.size() == 9){
//            assertTrue(true);
//        } else {
//            assertFalse(true);
//        }
//    }
}
