package tests.controller;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.GregorianCalendar;
import model.Profile;
import model.Tweet;
import model.User;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import org.junit.Test;
import service.ProfileService;
import service.TweetService;
import service.UserService;

public class UserControllerTest {
   
//    private final UserService userService;
//    private final ProfileService profileService;
//    private final TweetService tweetService;
//    
//    public UserControllerTest() {
//       this.userService = new UserService();
//       this.profileService = new ProfileService();
//       this.tweetService = new TweetService();
//    }
//
//    /**
//     * If Two instances are made for either user and profile 
//     * One condition: User id needs to be in the profile user id to force a relationship
//     * 
//     * When both conditions are met the test will succeed
//     */
//    @Test
//    public void registerUserTest() {
//        User user = new User("username","email@email.com","password","type","firstname","lastname", new Date());
//        Profile profile = new Profile(user.getId(), "biography", "location","website", "../avatar.jpg","../image.jpg");
//        userService.addUser(user);
//        profileService.addProfile(profile);
//    }
//    
//    /**
//     * Succeeds when there is a duplicate email available
//     */
//    @Test(expected = IllegalArgumentException.class) 
//    public void registerUserDuplicateEmailTest(){
//        User user = new User("username","email@email.com1","password","type","firstname","lastname", new Date());
//        Profile profile = new Profile(user.getId(), "biography", "location","website", "../avatar.jpg","../image.jpg");
//        userService.addUser(user);
//        profileService.addProfile(profile);
//    }
//    
//    /**
//     * Expect an invalid email format, this will succeed
//     */ 
//    @Test(expected = IllegalArgumentException.class) 
//    public void registerUserInvalidEmailTest(){
//        User user = new User("username","abcdefg","password","type","firstname","lastname", new Date());
//        Profile profile = new Profile(user.getId(), "biography", "location","website", "../avatar.jpg","../image.jpg");
//        userService.addUser(user);
//        profileService.addProfile(profile);
//    }
//    
//    /**
//     * Succeeds when there is an duplicate username
//     */
//    @Test(expected = IllegalArgumentException.class) 
//    public void registerUserDuplicateNameTest(){
//        User user = new User("username1","email@email.com","password","type","firstname","lastname", new Date());
//        Profile profile = new Profile(user.getId(), "biography", "location","website", "../avatar.jpg","../image.jpg");
//        userService.addUser(user);
//        profileService.addProfile(profile);
//    }
//    
//    /**
//     * Test if an user can login. Email and password must be equal
//     */
//    @Test
//    public void loginUserTest(){
//        assertTrue(userService.getUsers().get(0).Login("email1@email.com", "password1"));
//    }
//    
//    /**
//     * Test if an user cannot login if email and password doesn't match
//     */
//    
//    @Test(expected = AssertionError.class) 
//    public void loginUserFailTest(){
//        assertTrue(userService.getUsers().get(0).Login("email@email.com2", "password1"));
//    }
//    
//    /**
//     * Test if an user can logout.
//     */
//    @Test
//    public void logoutUserTest(){
//        assertTrue(userService.getUsers().get(0).Logout());
//    }
//    
//    /**
//     * Test if you can get a profile
//     */
//    @Test
//    public void getProfileTest(){
//        Profile profile = new Profile(userService.getUsers().get(0).getId(), "biography", "location","website", "../avatar.jpg","../image.jpg");
//        userService.getUsers().get(0).setProfile(profile);
//        if(userService.getUsers().get(0).getProfile() instanceof Profile){
//            assertTrue(true);
//        } else{
//            assertFalse(true);
//        }
//    }
//    
//    /**
//     * Test if an user got no profile, succeeds if this is true
//     */
//    @Test
//    public void getNonExistingProfileTest(){
//        Profile profile = userService.getUsers().get(0).getProfile();
//        if(userService.getUsers().get(0).getProfile() instanceof Profile){
//            assertFalse(true);
//        } else{
//            assertTrue(true);
//        }
//    }   
//    
//    /**
//     * Gets the latest 10 tweets from an user
//     * if exactly 10 tweets are returned the test will succeed else fail
//     */
//    @Test
//    public void getLatestTweetsTest() {
//        userService.getUsers().get(0).setTweets(tweetService.getTweets());
//        if(userService.getUsers().get(0).getTweets().size() == 10){
//            assertTrue(true);
//        } else {
//            assertFalse(true);
//        }
//    }
//    
//    /**
//     * The user doesn't have any tweets
//     */
//    public void getLatestTweetsButGotNoneTest(){
//        if(userService.getUsers().get(0).getTweets().isEmpty()){
//            assertTrue(true);
//        } else {
//            assertFalse(true);
//        }
//    }
//    
//    /**
//     * Edit the username of an user
//     * If the username got changed to choisen input for name the test succeeded
//     */
//    @Test
//    public void editUserNameTest(){
//        userService.getUsers().get(0).setUsername("TheTweetMaster");
//        if(userService.getUsers().get(0).getUsername().equals("TheTweetMaster")){
//            assertTrue(true);
//        } else {
//            assertFalse(true);
//        }
//    }
//    
//    /**
//     * Edit the username of an user
//     * If the username got changed to choisen input for name the test succeeded
//     */
//    @Test
//    public void editUserNameDuplicateTest(){
//        userService.getUsers().get(0).setUsername("username1");
//        for(int i = 0; i < userService.getUsers().size(); i++){
//          if(userService.getUsers().get(i).getUsername().equals(userService.getUsers().get(0))){
//             assertTrue(true);
//          }
//        }
//    }
//    
//    /**
//     * Gets selected follower if a user is returned it succeeds
//     */
//    @Test
//    public void getFollowersTest(){
//        ArrayList<User> followers = new ArrayList();
//        followers.add(userService.getUsers().get(0));
//        followers.add(userService.getUsers().get(1));
//        userService.getUsers().get(0).setFollowers(followers);
//        
//        if(userService.getUsers().get(0).getFollowers().size() == 2){
//            assertTrue(true);
//        } else {
//            assertFalse(true);
//        }
//    }
//    
//    /**
//     * Got no followers
//     */
//    @Test
//    public void getFollowersButGotNoneTest(){
//        if(userService.getUsers().get(0).getFollowers().isEmpty()){
//            assertTrue(true);
//        } else {
//            assertFalse(true);
//        }
//    }
//    
//    /**
//     * Gets selected following if a user is returned it succeeds
//     */
//    @Test
//    public void getFollowingTest(){
//        ArrayList<User> following = new ArrayList();
//        following.add(userService.getUsers().get(0));
//        following.add(userService.getUsers().get(1));
//        userService.getUsers().get(0).setFollowing(following);
//        
//        if(userService.getUsers().get(0).getFollowing().size() == 2){
//            assertTrue(true);
//        } else {
//            assertFalse(true);
//        }
//    }
//    
//    /**
//     * Got no followings
//     */
//    @Test
//    public void getFollowingButGotNoneTest(){
//        if(userService.getUsers().get(0).getFollowing().isEmpty()){
//            assertTrue(true);
//        } else {
//            assertFalse(true);
//        }
//    }
//    
//    /**
//     * Adds a following to your list so you follow a new user
//     */
//    @Test
//    public void addFollowingTest(){
//        ArrayList<User> following = new ArrayList();
//        following.add(userService.getUsers().get(0));
//        userService.getUsers().get(0).setFollowing(following);
//        
//        if(userService.getUsers().get(0).getFollowing().size() == 1){
//            assertTrue(true);
//        } else {
//            assertFalse(true);
//        }
//    }
//    
//    /**
//     * Search an user if found more than 0 users this function will succeed
//     */
//    @Test
//    public void searchUserTest(){
//        ArrayList<User> searchedUsers = (ArrayList<User>) userService.searchUsers("username2");
//        if(searchedUsers.size() > 0){
//            assertTrue(true);
//        } else {
//            assertFalse(true);
//        }
//    }
//    
//    /**
//     * Search an user but found none
//     */
//    @Test
//    public void searchUserButFoundNoneTest(){
//        ArrayList<User> searchedUsers = (ArrayList<User>) userService.searchUsers("weirdo");
//        if(searchedUsers.isEmpty()){
//            assertTrue(true);
//        } else {
//            assertFalse(true);
//        }
//    }
//    
//     /**
//     * Removes an existing follower if array is empty after that test succeeds
//     */
//    @Test
//    public void removeFollowingTest(){ 
//        ArrayList<User> following = new ArrayList();
//        following.add(userService.getUsers().get(0));
//        userService.getUsers().get(0).setFollowing(following);
//        userService.getUsers().get(0).getFollowing().remove(0);
//        
//        if(userService.getUsers().get(0).getFollowing().isEmpty()){
//            assertTrue(true);
//        } else {
//            assertFalse(true);
//        }
//    }
//    
//    /**
//     * Trying to remove an object that does not exist in the array
//     * If exception is IndexOutOfBoundsException thrown the test succeeds
//     */
//    @Test(expected = IndexOutOfBoundsException.class) 
//    public void removeFollowingButDoesntExistTest(){
//        userService.getUsers().get(0).getFollowing().remove(666);
//    }
//    
//    /**
//     * User get his tweets and all tweets from following users
//     */
//    @Test
//    public void getTimelineTest(){
//        ArrayList<Tweet> timelineTweets = new ArrayList();
//        ArrayList<User> following = new ArrayList();
//        ArrayList<Tweet> myTweets = new ArrayList();
//        ArrayList<Tweet> otherTweets = new ArrayList();
//        following.add(userService.getUsers().get(0));
//        following.add(userService.getUsers().get(1));
//        myTweets.add(tweetService.getTweets().get(0));
//        myTweets.add(tweetService.getTweets().get(1));
//        otherTweets.add(tweetService.getTweets().get(2));
//        otherTweets.add(tweetService.getTweets().get(3));
//        userService.getUsers().get(0).setFollowing(following); 
//        userService.getUsers().get(0).setTweets(otherTweets);
//        timelineTweets.add(myTweets.get(0));
//        timelineTweets.add(myTweets.get(1));
//        
//        for(int i = 0; i < following.size(); i++){
//            for(int y = 0; y < following.get(i).getTweets().size(); y++){
//                timelineTweets.add(following.get(i).getTweets().get(y));
//            }
//        }
//        
//        timelineTweets.get(0).setCreated_at(new GregorianCalendar(2014, Calendar.FEBRUARY, 11).getTime());
//        timelineTweets.get(1).setCreated_at(new GregorianCalendar(2015, Calendar.FEBRUARY, 11).getTime());
//        timelineTweets.get(2).setCreated_at(new GregorianCalendar(2018, Calendar.FEBRUARY, 11).getTime());
//        timelineTweets.get(3).setCreated_at(new GregorianCalendar(2020, Calendar.FEBRUARY, 11).getTime());
//        
//        Collections.sort(timelineTweets, Collections.reverseOrder());
//        for(int i = 0; i < timelineTweets.size(); i++){
//            System.out.println(timelineTweets.get(i).getContent());
//            System.out.println(timelineTweets.get(i).getCreated_at());
//            System.out.println();
//        }
//        if(timelineTweets.size() == 4){
//            assertTrue(true);
//        }
//    }
//    
//    /**
//     * Test if an user can get his mentions as tweets
//     */
//    @Test
//    public void getMentionsTest(){
//        ArrayList<Tweet> mentions = new ArrayList();
//        userService.getUsers().get(0).setMentions(tweetService.getTweets());
//        userService.getUsers().get(0).getMentions();
//    }
//    
//    /**
//     * If new user type matched string admin, admin is set
//     */
//    @Test
//    public void setUserRoleTest(){
//        userService.getUsers().get(0).setType("admin");
//        if("admin".equals(userService.getUsers().get(0).getType())){
//           assertTrue(true); 
//        } else {
//           assertFalse(true); 
//        }
//    } 
}
