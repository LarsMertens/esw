package tests.dao;

import dao.ProfileDao;
import dao.ProfileDaoImp;
import model.Profile;
import org.junit.Test;
import static org.junit.Assert.*;

public class ProfileDaoImpTest {
   
    private final ProfileDao profileDao;
   
    public ProfileDaoImpTest() {
        profileDao = new ProfileDaoImp();
    }

    @Test
    public void initProfileTest() {
        if(profileDao.findAll().size() == 10){
            assertTrue(true);
        } else {
            assertFalse(true);
        }
    }
    
    @Test
    public void createTest() {
        //Profile profile = new Profile(11L, "biography", "location","website", "../avatar.jpg","../image.jpg");
       //profileDao.create(profile);
        if(profileDao.findAll().size() == 11 && profileDao.find(11L) instanceof Profile){
            assertTrue(true);
        } else {
            assertFalse(true);
        }
    }
    
    @Test(expected = IllegalArgumentException.class) 
    public void createNullTest() {
        Profile profile = null;
        profileDao.create(profile);
        if(profileDao.findAll().size() == 11 && profileDao.find(11L) instanceof Profile){
            assertTrue(true);
        } else {
            assertFalse(true);
        }
    }
    
    @Test
    public void findAllTest() {
        if(profileDao.findAll().size() == 10){
            assertTrue(true);
        } else {
            assertFalse(true);
        }
    }
    
    @Test
    public void findTest() {
        if(profileDao.find(1L) instanceof Profile){
            assertTrue(true);
        } else {
            assertFalse(true);
        }
    }
    
    @Test(expected = IllegalArgumentException.class) 
    public void findFailTest() {
        if(profileDao.find(666L) instanceof Profile){
            assertTrue(true);
        } else {
            assertFalse(true);
        }
    }  
}
