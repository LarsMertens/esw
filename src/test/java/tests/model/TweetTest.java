package tests.model;
import dao.TweetDao;
import dao.TweetDaoImp;
import java.util.ArrayList;
import model.Tweet;
import org.junit.Test;
import static org.junit.Assert.*;

public class TweetTest {
   
    private final TweetDao tweetDao;
    
    public TweetTest() {
        tweetDao = new TweetDaoImp();
    }
 
    /**
     * Will be implemented in v3
     */
    @Test
    public void getMentions(){
        assertTrue(true);    
    }
    
    /**
     * Will be implemented in v3
     */
    @Test
    public void getTrends(){
        assertTrue(true);
    }
    
    /**
     * Will be implemented in v3
     */
    @Test
    public void removeTweet(){
        ArrayList<Tweet> tweets = (ArrayList<Tweet>) tweetDao.findAll();
        tweets.remove(tweets.get(0));
        if(tweets.size() == 9){
            assertTrue(true);
        } else {
            assertFalse(true);
        }
    }
}
